/**
 * @interface ILiveConsoleOptions Represents the configuration of the LiveConsole.
 */
export interface ILiveConsoleOptions {
  /**
   * @property Defines the maximum depth the LiveConsole can display the properties of logged objects.
   */
  maximumObjectDepth?: number;

  /**
   * @property Defines the maximum number of displayed entries of the logged objects.
   */
  maximumObjectEntries?: number;

  /**
   * @property Defines the maximum number of displayed entries of logged arrays.
   */
  maximumArrayEntries?: number;

  /**
   * @property If enabled, similar logs will be stacked (by just incrementing a counter) instead of being added to the logs list.
   */
  stackSimilarLogs?: boolean;

  /**
   * @property Defines the HTML element id of your web page that will contain the log entries.
   */
  consoleElementId?: string;

  /**
   * @property If enabled, overwritable logs will behave like if they are stacked (by incrementing a counter).
   */
  countOverwritableLogIterations?: boolean;

  /**
   * @property If enabled, the native browser console functions will be overriden, and logs will be both visible in the native console and
   * the Live Console container. Note that if you use several instances of LiveConsole, only the last one that has this setting set to
   * true will be used to override the native console functions, and turning off this setting on one of the instances will reset the native
   * console functions until the setting is turned on again.
   */
  overrideNativeConsole?: boolean;
}
