import { ILiveConsoleOptions } from './live-console-options';

export const DEFAULT_CONFIG: ILiveConsoleOptions = {
  maximumObjectDepth: 3,
  maximumObjectEntries: 50,
  maximumArrayEntries: 100,
  stackSimilarLogs: true,
  consoleElementId: 'live-console',
  countOverwritableLogIterations: true,
  overrideNativeConsole: false
};

export const NATIVE_CONSOLE = {
  info: console.info,
  log: console.log,
  warn: console.warn,
  error: console.error,
  clear: console.clear
};
