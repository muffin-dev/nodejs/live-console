import { EVerbosity, LiveConsole } from './live-console';
import { ILiveConsoleOptions } from './live-console-options';

// Store the main instance of LiveConsole.
let _liveConsole: LiveConsole = null;
function liveConsole(): LiveConsole {
  if (_liveConsole === null) {
    _liveConsole = new LiveConsole();
  }
  return _liveConsole;
}

/**
 * Adds a log entry with "info" verbosity.
 * @param data The data to log.
 */
export function info(...data: unknown[]): void {
  liveConsole().addEntry(EVerbosity.Info, null, ...data);
}

/**
 * Adds a log entry with "log" verbosity.
 * @param data The data to log.
 */
export function log(...data: unknown[]): void {
  liveConsole().addEntry(EVerbosity.Log, null, ...data);
}

/**
 * Adds a log entry with the "warning" verbosity.
 * @param data The data to log.
 */
export function warn(...data: unknown[]): void {
  liveConsole().addEntry(EVerbosity.Warning, null, ...data);
}
/**
 * Adds a log entry with the "error" verbosity.
 * @param data The data to log.
 */
export function error(...data: unknown[]): void {
  liveConsole().addEntry(EVerbosity.Error, null, ...data);
}

/**
 * Creates an overwritable log with the given verbosity. You should use overwrite() for updating this log entry in the future.
 * @param name The name of the overwritable log entry to create. If there's already a log entry with this name, it will be updated.
 * @param verbosity The verbosity level of the log entry. Ignored if there's already a log entry with the same name.
 * @param data The data to log. If there's already a log entry with the same name, these data will replace the existing ones.
 */
export function write(name: string, verbosity: EVerbosity, ...data: unknown[]): void {
  liveConsole().addEntry(verbosity, name, ...data);
}

/**
 * Overwrites a named log entry. If no entry of the given name can be found, creates one with the given data, and the "log" verbosity.
 * @param name The name of the overwritable log entry.
 * @param data The data to log, replacing the existing log data.
 */
export function overwrite(name: string, ...data: unknown[]): void {
  liveConsole().addEntry(EVerbosity.Info, name, ...data);
}

/**
 * Clears the Live Console log elements and the logs history.
 */
export function clear(): void {
  liveConsole().clear();
}

/**
 * Defines the configuration for the main LiveConsole instance.
 * @param options The configuration to use.
 */
export function config(options: ILiveConsoleOptions): void {
  liveConsole().config(options);
}

/**
 * Adds an event listener for the named event.
 * @param eventName The name of the event to listen.
 * @param callback The listener to bind to the named event.
 */
export function on(eventName: string | symbol, callback: (...params: unknown[]) => void): void {
  liveConsole().on(eventName, callback);
}

/**
 * Removes a callback from the named event listeners list.
 * @param eventName The name of the event of which you want to remove the listener.
 * @param callback The callback you want to remove. Note that if you use anonymous methods, the listener won't be found in the list.
 * @returns Returns true if the callback exists in the named event's listeners list and has been removed successfully, otherwise false.
 */
export function removeListener(eventName: string | symbol, callback: (...params: unknown[]) => void): void {
  liveConsole().removeListener(eventName, callback);
}

export { LiveConsole, EVerbosity } from './live-console';
