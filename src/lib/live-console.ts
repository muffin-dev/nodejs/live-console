/* eslint-disable max-statements-per-line */
import { EventsManager, overrideObject } from '@muffin-dev/js-helpers';
import { DEFAULT_CONFIG, NATIVE_CONSOLE } from './const';
import { ILiveConsoleOptions } from './live-console-options';

export enum EVerbosity {
  Info = 'info',
  Log = 'log',
  Warning = 'warn',
  Error = 'error',
}

/**
 * @interface ILog Represents a log item of the LiveConsole.
 */
interface ILog {
  // The eventual name of this log entry (for using overwritable logs)
  name?: string;
  // The verbosity of this log
  verbosity: EVerbosity;
  // The displayed data of this log (any alteration after logging the information won't be displayed though)
  data: unknown;
  // The HTML Element created to display the log entry
  element?: HTMLElement;
  // The eventual number of iterations of this log (used only if this log entry is not overwritable)
  iterations: number;
  // The date when this log entry has been created
  date: Date;
}

export class LiveConsole {

  //#region Properties

  private _consoleEvents = new EventsManager();
  private _consoleElement: HTMLElement = null;
  private _history = new Array<ILog>();
  private _pageInitialized = false;
  private _config: ILiveConsoleOptions = null;

  /**
   * Gets the HTML element where logs of LiveConsole are displayed.
   */
  private get consoleElement(): HTMLElement {
    return this._consoleElement;
  }

  //#endregion

  //#region Initialization

  constructor(config: ILiveConsoleOptions = null) {
    this._config = Object.assign({ }, DEFAULT_CONFIG);
    this.config(config);

    if (document.readyState === 'complete') {
      this._init();
    }
    else {
      window.addEventListener('load', () => {
        this._init();
      });
    }
  }

  private _init() {
    this._pageInitialized = true;
    this._findConsoleElement();
  }

  //#endregion

  //#region Public API

  /**
   * Adds a log entry with the lowest verbosity.
   * @param data The data to log.
   */
  public info(...data: unknown[]): void {
    this.addEntry(EVerbosity.Info, null, ...data);
  }

  /**
   * Adds a log entry with "log" verbosity.
   * @param data The data to log.
   */
  public log(...data: unknown[]): void {
    this.addEntry(EVerbosity.Log, null, ...data);
  }

  /**
   * Adds a log entry with the "warning" verbosity.
   * @param data The data to log.
   */
  public warn(...data: unknown[]): void {
    this.addEntry(EVerbosity.Warning, null, ...data);
  }

  /**
   * Adds a log entry with the "error" verbosity.
   * @param data The data to log.
   */
  public error(...data: unknown[]): void {
    this.addEntry(EVerbosity.Error, null, ...data);
  }

  /**
   * Creates an overwritable log with the given verbosity. You should use overwrite() for updating this log entry in the future.
   * @param name The name of the overwritable log entry to create. If there's already a log entry with this name, it will be updated.
   * @param verbosity The verbosity level of the log entry. Ignored if there's already a log entry with the same name.
   * @param data The data to log. If there's already a log entry with the same name, these data will replace the existing ones.
   */
  public write(name: string, verbosity: EVerbosity, ...data: unknown[]): void {
    this.addEntry(verbosity, name, ...data);
  }

  /**
   * Overwrites a named log entry. If no entry of the given name can be found, creates one with the given data, and the "log" verbosity.
   * @param name The name of the overwritable log entry.
   * @param data The data to log, replacing the existing log data.
   */
  public overwrite(name: string, ...data: unknown[]): void {
    this.addEntry(EVerbosity.Log, name, ...data);
  }

  /**
   * Adds a new log entry to the console history, and displays it if possible.
   * @param verbosity The verbosity of the log entry.
   * @param name The name of the log entry (if not null, makes the log overwritable).
   * @param data The data to display for this log entry.
   */
  public addEntry(verbosity: EVerbosity, name: string, ...data: unknown[]): void {
    const parsedData = (data && data.length === 1) ? data[0] : data;
    const logEntry: ILog = {
      name: name || null,
      verbosity,
      data: parsedData,
      date: new Date(),
      element: null,
      iterations: 1
    };

    let registered = false;
    if (logEntry.name !== null) {
      for (const log of this._history) {
        if (log.name === logEntry.name) {
          log.date = logEntry.date;
          log.data = logEntry.data;
          if (this._config.countOverwritableLogIterations) {
            log.iterations++;
          }
          this._updateLog(log);
          registered = true;
          break;
        }
      }
    }

    if (!registered) {
      // If stack option is enabled, increment the iterations number if needed
      if (this._config.stackSimilarLogs) {
        for (const log of this._history) {
          if (log.data === parsedData) {
            log.iterations++;
            this._updateIterations(log);
            registered = true;
          }
        }
      }
    }

    // Saves and display the log if it has not been registered yet
    if (!registered) {
      this._history.push(logEntry);
      this._displayLog(logEntry);
    }

    this._consoleEvents.emit('log', parsedData);
  }

  /**
   * Adds an event listener for the named event.
   * @param eventName The name of the event to listen.
   * @param callback The listener to bind to the named event.
   */
  public on(eventName: string | symbol, callback: (...params: unknown[]) => void): void {
    this._consoleEvents.on(eventName, callback);
  }

  /**
   * Removes a callback from the named event listeners list.
   * @param eventName The name of the event of which you want to remove the listener.
   * @param callback The callback you want to remove. Note that if you use anonymous methods, the listener won't be found in the list.
   * @returns Returns true if the callback exists in the named event's listeners list and has been removed successfully, otherwise false.
   */
  public removeListener(eventName: string | symbol, callback: (...params: unknown[]) => void): boolean {
    return this._consoleEvents.removeListener(eventName, callback);
  }

  /**
   * Clears the Live Console log elements and the logs history.
   */
  public clear(): void {
    for (const log of this._history) {
      if (log.element) {
        log.element.remove();
      }
    }

    this._history = new Array<ILog>();
  }

  /**
   * Defines the configuration for this LiveConsole instance.
   * @param options The configuration to use.
   */
  public config(options: ILiveConsoleOptions): void {
    overrideObject(this._config, options);

    if (this._config.overrideNativeConsole) {
      console.info = (...data: unknown[]) => { this.info(...data); NATIVE_CONSOLE.info(...data); };
      console.log = (...data: unknown[]) => { this.log(...data); NATIVE_CONSOLE.log(...data); };
      console.warn = (...data: unknown[]) => { this.warn(...data); NATIVE_CONSOLE.warn(...data); };
      console.error = (...data: unknown[]) => { this.error(...data); NATIVE_CONSOLE.error(...data); };
      console.clear = () => { this.clear(); NATIVE_CONSOLE.clear(); };
    }
    else {
      console.info = NATIVE_CONSOLE.info;
      console.log = NATIVE_CONSOLE.log;
      console.warn = NATIVE_CONSOLE.warn;
      console.error = NATIVE_CONSOLE.error;
    }
  }

  //#endregion

  //#region Private methods

  /**
   * Displays the given log entry if it's not already displayed and if the console element is valid.
   * @param log The log you want to display.
   */
  private _displayLog(log: ILog) {
    if (!log.element && this._pageInitialized && this.consoleElement) {

      // <div class="entry verbosity">
      //   <div class="iterations">
      //     <span>1</span>
      //   </div>
      //   <ul>
      //     <li>Log value 1</li>
      //     <li>Log value 2</li>
      //   </ul>
      // </div>

      const logContainer = document.createElement('div');
      log.element = logContainer;
      logContainer.className = `entry ${log.verbosity}`;

      const iterationsDiv = document.createElement('div');
      iterationsDiv.className = 'iterations';
      logContainer.append(iterationsDiv);
      const iterationsValue = document.createElement('span');
      iterationsValue.textContent = log.iterations.toString();
      iterationsValue.style.display = log.iterations > 1 ? 'inline' : 'none';
      iterationsDiv.append(iterationsValue);

      const ul = document.createElement('ul');
      logContainer.append(ul);
      this._generateElementForValue(ul, log.data);

      this.consoleElement.append(logContainer);
    }
  }

  /**
   * Generates the elements to the parent element in order to display the given value.
   * @param parent The parent element on which you want to append the display elements.
   * @param value The value for which you want to create the display elements.
   * @param skipLabel If true, it won't create a sub-list to display object entries or array keys.
   * @param objectDepth Defines the current depth of the displayed object. If it reaches the maximum depth, the operation is cancelled.
   * @returns Returns the generated element.
   */
  private _generateElementForValue(parent: HTMLElement, value: unknown, skipLabel = false, objectDepth = 0): HTMLElement {
    const li: HTMLElement = document.createElement('li');
    if (objectDepth >= this._config.maximumObjectDepth) {
      this._appendText(li, '...');
      parent.append(li);
      return li;
    }

    const hasLabel = this._appendLabel(li, value);
    if (!hasLabel) {
      // Else, if the value contains an object or an array
      if (typeof value === 'object') {
        // If the value is an array
        if (Array.isArray(value)) {
          let itemsParent = parent;
          // Create a sublist if required
          if (!skipLabel) {
            const foldable = this._appendFoldable(li, true);
            this._appendSpan(foldable, 'keyword array', 'Array');
            this._appendText(foldable, ` [${value.length}]`);
            itemsParent = document.createElement('ol');
            li.append(itemsParent);
          }
          // Display the content of each item of the array
          const count = Math.min(value.length, this._config.maximumArrayEntries);
          for (let i = 0; i < count; i++) {
            this._generateElementForValue(itemsParent, value[i]);
          }
          if (value.length > this._config.maximumArrayEntries) {
            const etcLi = document.createElement('li');
            etcLi.textContent = '...';
            itemsParent.append(etcLi);
          }
        }

        // Else, if the value is actually an object
        else {
          const entries = Object.entries(value);
          let itemsParent = parent;
          // Create a sub list element if required
          if (!skipLabel) {
            const foldable = this._appendFoldable(li, true);
            this._appendSpan(foldable, 'keyword object', 'Object');
            this._appendText(foldable, ` [${entries.length}]`);
            if (entries.length > 0) {
              itemsParent = document.createElement('ul');
              li.append(itemsParent);
            }
          }
          // For each object key
          let i = 0;
          for (const [ key, value ] of entries) {
            const subItem = document.createElement('li');
            if (i >= this._config.maximumObjectEntries) {
              subItem.textContent = '...';
              itemsParent.append(subItem);
              break;
            }

            const hasLabel = this._hasLabel(value);
            // If the value is simple (so, not an object, and not an array)
            if (hasLabel) {
              this._appendSpan(subItem, 'key', `${key}`);
              this._appendText(subItem, ': ');
              this._appendLabel(subItem, value);
            }
            // Else, if the value is complex
            else {
              // If the value is an object, display values recursively
              if (typeof value === 'object') {
                let subList: HTMLElement = null;

                if (Array.isArray(value)) {
                  const foldable = this._appendFoldable(subItem, true);
                  this._appendSpan(foldable, 'key', `${key}`);
                  this._appendText(foldable, ' (');
                  this._appendSpan(foldable, 'keyword', 'Array');
                  this._appendText(foldable, ` [${value.length}]):`);
                  subList = document.createElement('ol');
                }
                else {
                  const foldable = this._appendFoldable(subItem, true);
                  this._appendSpan(foldable, 'key', `${key}`);
                  this._appendText(foldable, ' (');
                  this._appendSpan(foldable, 'keyword', 'Object');
                  this._appendText(foldable, ` [${Object.keys(value).length}]):`);
                  subList = document.createElement('ul');
                }

                subItem.append(subList);
                this._generateElementForValue(subList, value, true, objectDepth + 1);
              }
              // Else, if the value can't be logged using the LiveConsole
              else {
                this._appendSpan(subItem, 'error', 'Not displayable');
              }
            }
            itemsParent.append(subItem);
            i++;
          }
        }
      }

      // Else, if the object can't be logged using the LiveConsole
      else {
        this._appendSpan(li, 'error', 'Not displayable');
      }
    }

    if (li.textContent !== '') {
      parent.append(li);
      return li;
    }
    return null;
  }

  private _appendSpan(parent: HTMLElement, className: string, content: string) {
    const span = document.createElement('span');
    span.className = className;
    span.textContent = content;
    parent.append(span);
  }

  private _appendText(parent: HTMLElement, content: string) {
    if (content === '') {
      return;
    }

    const text = document.createTextNode(content);
    parent.append(text);
  }

  private _appendFoldable(element: HTMLElement, folded = false): HTMLDivElement {
    element.classList.add('foldable');
    if (folded) {
      element.classList.add('folded');
    }

    const foldableLabel = document.createElement('div');
    foldableLabel.className = 'foldable-label';

    const arrow = document.createElement('div');
    arrow.className = 'arrow';
    foldableLabel.append(arrow);

    foldableLabel.addEventListener('click', () => {
      if (element.classList.contains('folded')) {
        element.classList.remove('folded');
      }
      else {
        element.classList.add('folded');
      }
    });

    element.append(foldableLabel);
    return foldableLabel;
  }

  /**
   * Checks if the given value has a displayable label.
   * @param value The value of which you want to display the label.
   * @returns Returns true if the value has a displayable label, otherwise false.
   */
  private _hasLabel(value: unknown): boolean {
    if (value === null || value === undefined) {
      return true;
    }

    if (typeof value === 'object') {
      return false;
    }

    return true;
  }

  /**
   * Appends the displayable label elements of the value to the given parent.
   * @param parent The parent element on which you want to append the label elements.
   * @param value The value of which you want to display the label.
   * @returns Returns true if the value has a displayable label and so elements have append successfully, otherwise false.
   */
  private _appendLabel(parent: HTMLElement, value: unknown): boolean {
    // If the value is null or undefined
    if (value === null || value === undefined) {
      const label = value === null ? 'null' : 'undefined';
      this._appendSpan(parent, `keyword ${label}`, label);
      return true;
    }

    const typeName = typeof value;
    if (typeof value === 'function') {
      this._appendSpan(parent, 'keyword function', 'Function');
      this._appendLabel(parent, ` ${(value.name ? value.name : '[anonymous]')}()`);
      return true;
    }
    else if (typeName !== 'object') {
      this._appendText(parent, value.toString());
      return true;
    }

    return false;
  }

  /**
   * Finds the first element with the "live-console" ID.
   */
  private _findConsoleElement(): HTMLElement {
    if (!this._consoleElement && this._pageInitialized) {
      this._consoleElement = document.querySelector(`#${this._config.consoleElementId}`);
      if (this._consoleElement !== null) {
        for (const log of this._history) {
          this._displayLog(log);
        }
      }
    }
    return this._consoleElement || null;
  }

  private _updateLog(log: ILog) {
    if (log.element) {
      this._updateIterations(log);
      log.element.querySelector('ul').remove();

      const ul = document.createElement('ul');
      log.element.append(ul);
      this._generateElementForValue(ul, log.data);
    }
  }

  private _updateIterations(log: ILog) {
    if (log.element && log.iterations > 1) {
      const iterationsValue = log.element.querySelector('.iterations span') as HTMLSpanElement;
      iterationsValue.style.display = 'inline';
      iterationsValue.textContent = (log.iterations < 1000) ? log.iterations.toString() : '999+';
    }
  }

  //#endregion

}
