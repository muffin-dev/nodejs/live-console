#!/usr/bin/env node

import path from 'path';
import { asAbsolute, readFileAsync, writeFileAsync } from '@muffin-dev/node-helpers';

const DEFAULT_LIB_PATH = path.join(process.cwd(), './live-console.min.js');
const LIB_URL_TAG = '%lib_url%';
const STYLE_URL_TAG = '%style_url%';
const DEFAULT_GENERATED_HTML_FILE_NAME = 'index.html';
const TEMPLATE_NAME_SEPARATOR = ':';

const TEMPLATE_PATHS: Record<string, string> = {
  console: path.join(__dirname, '../templates/console.html')
};
const STYLE_PATHS: Record<string, string> = {
  default: path.join(__dirname, '../styles/default.css')
};

const DEFAULT_TEMPLATE_PATH = TEMPLATE_PATHS.console;
const DEFAULT_STYLE_PATH = STYLE_PATHS.default;

async function exec(): Promise<void> {
  if (process.argv.length <= 2 || !process.argv[2].startsWith('generate')) {
    return;
  }

  const split = process.argv[2].split(TEMPLATE_NAME_SEPARATOR);
  const templatePath = TEMPLATE_PATHS[split[1]] || DEFAULT_TEMPLATE_PATH;

  let html = await readFileAsync(templatePath);
  let distPath = process.argv[3] ? asAbsolute(process.argv[3]) : process.cwd();
  if (!distPath.endsWith('.html')) {
    distPath = path.join(distPath, DEFAULT_GENERATED_HTML_FILE_NAME);
  }

  const libPath = process.argv[4] ? asAbsolute(process.argv[4]) : DEFAULT_LIB_PATH;
  const stylePath = process.argv[5] ? asAbsolute(process.argv[5]) : DEFAULT_STYLE_PATH;

  html = html.replace(LIB_URL_TAG, path.relative(path.dirname(distPath), libPath));
  html = html.replace(STYLE_URL_TAG, path.relative(path.dirname(distPath), stylePath));
  await writeFileAsync(distPath, html);
}

exec();
