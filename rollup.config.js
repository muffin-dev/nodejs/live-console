import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import minify from 'rollup-plugin-babel-minify';

export default [
  {
    input: 'index.js',
    output:
    {
      name: 'LiveConsole',
      file: './live-console.min.js',
      format: 'iife',
      sourcemap: 'inline'
    },
    plugins: [
      resolve(),
      commonjs(),
      minify({ comments: false }),
    ],
  }
];
