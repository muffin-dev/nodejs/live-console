# Muffin Dev for Node - Live Console

This library allow you to display a virtual console inside a web page, so you can debug efficiently.

![*Live Console* animated demo](./src/doc/images/live-console.gif)

## Key features

- Overwritable logs: update an existing log to avoid having a ton of lines displayed in your console
- No need to open your dev tools to see what's happening in your web page: you'll see it directly in that page
- Global instance to make it easy to use, or you can create multiple instances to display logs in several blocks in your page

### Future improvements

- Export to a log file

## Usage

There's two main contexts where you could use this library:

- You're making a website frontend
- You're in a Node JS environment in order to develop a web broswer library

### Usage for frontend developers

First, download the minified library file `live-console.min.js` at the root of [this repository](https://gitlab.com/muffin-dev/nodejs/live-console), and include it in your website page like a regular JS script.

Unless you want to create your own CSS style for *Live Console*, you can also download styles from the [`/styles` directory](./styles) of this repository, and import you CSS file with a `link` tag.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Live Console</title>

  <!-- Include the minified library file -->
  <script src="live-console.min.js"></script>
  <!-- Include the design of your choice -->
  <link rel="stylesheet" href="default.css">
</head>
<body>
  <!-- The element with id "live-console" will be used to display the logs of Live Console -->
  <div id="live-console" class="live-console"></div>

  <!-- This example script will log "Tick" using Live Console every second -->
  <script>
    setInterval(() => {
      LiveConsole.log('Tick');
    }, 1000);
  </script>
</body>
</html>
```

### Usage for Node JS developers

Install the library with *npm*:

```bash
npm install @muffin-dev/live-console
```

Then, import the module in order to use it in your scripts:

JavaScript example:

```js
const liveConsole = require('@muffin-dev/live-console');
liveConsole.log('Test');
```

TypeScript example:

```ts
import LiveConsole from '@muffin-dev/live-console';
LiveConsole.log('Test');
```

## Configuration

You can configure *Live Console* by using the `config()` method. It takes an object that represents the options you want to use.

Here is how to use the `config()` method, with the default configuration:

```js
liveConsole.config({
  // Defines the maximum depth the LiveConsole can display the properties of logged objects.
  maximumObjectDepth: 3,

  // Defines the maximum number of displayed entries of the logged objects.
  maximumObjectEntries: 50,

  // Defines the maximum number of displayed entries of logged arrays.
  maximumArrayEntries: 100,

  // If enabled, similar logs will be stacked (by just incrementing a counter) instead of being added to the logs list.
  stackSimilarLogs: true,

  // Defines the HTML element id of your web page that will contain the log entries.
  consoleElementId: 'live-console',

  // If enabled, overwritable logs will behave like if they are stacked (by incrementing a counter).
  countOverwritableLogIterations: true,

  // If enabled, the native browser console functions will be overriden, and logs will be both visible in the native console and the Live Console container.
  overrideNativeConsole: false,
});
```

## Public API

### Methods

#### `info()`

```js
function info(...data)
```

Adds a log entry with the lowest verbosity.

- `data: any[]`: The data to log.

#### `log()`

```js
function log(...data)
```

Adds a log entry with *log* verbosity.

- `data: any[]`: The data to log.

#### `warn()`

```js
function warn(...data)
```

Adds a log entry with *warn* verbosity.

- `data: any[]`: The data to log.

#### `error()`

```js
function error(...data)
```

Adds a log entry with *error* verbosity.

- `data: any[]`: The data to log.

#### `write()`

```js
function write(name, verbosity, ...data)
```

Creates an overwritable log with the given verbosity. You should use `overwrite()` for updating this log entry in the future.

- `name: string`: The name of the overwritable log entry to create. If there's already a log entry with this name, it will be updated..
- `verbosity: EVerbosity`: The verbosity level of the log entry. Ignored if there's already a log entry with the same name.
- `data: any[]`: The data to log. If there's already a log entry with the same name, these data will replace the existing ones.

#### `overwrite()`

```js
function overwrite(name, verbosity, ...data)
```

Overwrites a named log entry. If no entry of the given name can be found, creates one with the given data, and the *log* verbosity.

- `name: string`: The name of the overwritable log entry.
- `data: any[]`: The data to log. If there's already a log entry with the same name, these data will replace the existing ones.

#### `clear`

```js
function clear()
```

Clears the *Live Console* log elements and the history.

#### `config()`

```js
function config(options)
```

Defines the configuration for the main LiveConsole instance. See the *Configuration* section above for more informations.

- `options: ILiveConsoleOptions`: The configuration to use.

#### `on()`

```js
function on(eventName, callback)
```

Adds an event listener for the named event. See the *Events* section below for more informations.

- `eventName: string | symbol`: The name of the event to listen.
- `callback: (...params: any[]) => void`: The listener to bind to the named event.

#### `removeListener()`

```js
function removeListener(eventName, callback)
```

Removes a callback from the named event listeners list.

- `eventName: string | symbol`: The name of the event of which you want to remove the listener.
- `callback: (...params: any[]) => void`: The callback you want to remove. Note that if you use anonymous methods, the listener won't be found in the list.

Returns `true` if the callback exists in the named event's listeners list and has been removed successfully, otherwise `false`.

### Enumerations

#### `EVerbosity`

Represents the available log verbosity values.

```ts
enum EVerbosity {
  Info    = 'info',
  Log     = 'log',
  Warning = 'warn',
  Error   = 'error',
}
```

### Events

#### `log`

```js
LiveConsole.on('log', () => { });
```

Called each time the *Live Console* logs a new entry.

## Advanced: work with several consoles

You can create several instances of *Live Consoles*, in order to get logs in separate sections of your web pages. To do that, you'll need to use the `LiveConsole` class, instead of the global variable.

Usage example:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Live Console (multiple instances)</title>
  <script src="live-console.min.js"></script>
  <link rel="stylesheet" href="default.css">
  <style>
    .live-console {
      flex: 1;
      border: 3px double var(--separator);
    }
  </style>
</head>
<body>
  <div style="display: flex">
    <div id="console1" class="live-console"></div>
    <div id="console2" class="live-console"></div>
  </div>

  <script>
    const console1 = new LiveConsole.LiveConsole({ consoleElementId: 'console1' });
    const console2 = new LiveConsole.LiveConsole({ consoleElementId: 'console2' });

    console1.log('Test console 1');
    console2.log('Test console 2');
  </script>
</body>
</html>
```

![*Live Console* multiple instances preview](./src/doc/images/multiple-instances.jpg)

## License

This library uses the [Creative Commons Attribution-ShareAlike 4.0 International license (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

This means that you can use, share and modify the content as you want (even for commercial projects, but only under the following terms:

- Give an "appropriate credit" (name the authors), and write a link to this package
- If the source package is modified, you must distribute your contibutions under the same license ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode))